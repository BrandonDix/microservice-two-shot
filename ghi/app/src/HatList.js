import React from 'react';
import { Link } from 'react-router-dom';

function HatColumn(props) {
  return (
    <div className="col">
      {props.list.map(hat => {
        return (
          <div key={hat.href} className="card mb-3 shadow">
            <img src={hat.picture} className="card-img-top" />
            <div className="card-body">
              <h5 className="card-title">{hat.style_name}</h5>
              <h6 className="card-subtitle mb-2 text-muted">
                {hat.fabric}
              </h6>
              <h7 className="card-subtitle mb-2 text-muted">
                {hat.color}
              </h7>
              <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                <button onClick={() => deleteHat(hat)} type="button" className="btn btn-outline-danger">Delete</button>
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
}

async function deleteHat(hat){
  const deleteURL = `http://localhost:8090${hat.href}`
  const response = await fetch(deleteURL, {method: "delete"})
  if (response.ok) {
    console.log("hat deleted", response)
}
}
class HatList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hatColumns: [[], [], []],
    };
  }


  async componentDidMount() {
    const url = 'http://localhost:8090/api/hats/';

    try {
      const response = await fetch(url);
      if (response.ok) {
        // Get the list of hats
        const data = await response.json();

        // Create a list of for all the requests and
        // add all of the requests to it
        const requests = [];
        for (let hat of data.hats) {
          const detailUrl = `http://localhost:8090${hat.href}`;
          requests.push(fetch(detailUrl));
        }

        // Wait for all of the requests to finish
        // simultaneously
        const responses = await Promise.all(requests);

        // Set up the "columns" to put the hats
        // information into
        const hatColumns = [[], [], []];

        // Loop over the hat detail responses and add
        // each to to the proper "column" if the response is
        // ok
        let i = 0;
        for (const hatResponse of responses) {
          if (hatResponse.ok) {
            const details = await hatResponse.json();
            hatColumns[i].push(details);
            i = i + 1;
            if (i > 2) {
              i = 0;
            }
          } else {
            console.error(hatResponse);
          }
        }

        // Set the state to the new list of three lists of
        // conferences
        this.setState({ hatColumns: hatColumns });
      }
    } catch (e) {
      console.error(e);
    }
  }

  render() {
    return (
      <>
        <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
          <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="" width="600" />
          <h1 className="display-5 fw-bold">Hats!</h1>
          <div className="col-lg-6 mx-auto">
            <p className="lead mb-4">
              If you like hats.... well we got em all.
            </p>
          </div>
        </div>
        <div className="container">
          <h2>List of Hats</h2>
          <div className="row">
            {this.state.hatColumns.map((hatList, index) => {
              return (
                <HatColumn key={index} list={hatList} />
              );
            })}
          </div>
        </div>
      </>
    );
  }
}

export default HatList;